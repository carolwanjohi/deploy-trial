# Setting Up Django v.1.11 for Heroku Deployment

### Bitbucket Repo Link
- Eventually the application structure is like [this](https://bitbucket.org/carolwanjohi/deploy-trial/src/72509b16d400/)

To be safe,
TRY THIS ON A CLONE OF YOUR PROJECT!! 

* **Root directory** name : `rootDir`
* **Django-project** name : `myproject`
* **Django-app** name : `myapp`

## Setting Up the Application
```
mkdir rootDir
cd rootDir
virtualenv venv
source venv/bin/activate
pip3 install django==1.11 django-heroku gunicorn whitenoise python-decouple whitenoise django-bootstrap3
pip3 freeze > requirements.txt
```

```
touch .env
    SECRET_KEY=<>
    DEBUG=True
    DBNAME=<>
    DBUSER=<>
    DBPASSWORD=<>
```

```
touch .gitignore
    .DS_STORE
    venv/
    *.pyc
    .env
    db.sqlite3
```

```
django-admin startproject myproject .
```

* Add the following at the top of `myproject/settings.py`
```py
from decouple import config
import django_heroku
django_heroku.settings(locals(), staticfiles=False)
```

* Update `SECRET_KEY`, `DEBUG` and `ALLOWED_HOSTS` in `myproject/settings.py`
```py
SECRET_KEY = config('SECRET_KEY')
DEBUG = config('DEBUG',default=False, cast=bool)
ALLOWED_HOSTS = ['*']
```

* Add this to the `MIDDLEWARE` list in `myproject/settings.py`
```py
MIDDLEWARE = [
    .....
    'whitenoise.middleware.WhiteNoiseMiddleware',
]
```

* Update `DATABASES` in `myproject/settings.py`
```py
DATABASES = {
   'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': config('DBNAME'),
        'USER':config('USER'),
        'PASSWORD':config('PASSWORD')
   }
}
```

* Add this below `DATABASES` in `myproject/settings.py`
```py
import dj_database_url
db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)
```

* Add this at the bottom of `myproject/settings.py`
```py
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
```

```
touch Procfile
    web: gunicorn deploy.wsgi
```

```
touch runtime.txt
    python-3.6.2
```

```
mkdir static
cd static
mkdir css && mkdir js && mkdir img
```

Build a basic site with CSS, Templates and a simple Model class. Also, remember to migrate since the database will have a simple Model class. Once everything is running as expected, push your code to your remote repository(Github).

## Deploying to Heroku
After pushing your work to your remote repository do the follow

```
heroku login
heroku create *app-name*
```

* For `heroku config:set` ensure you set for all the variables in the `.env` file
```
heroku config:set SECRET_KEY=<>
heroku config:set DBNAME=<>
heroku config:set DBUSER=<>
heroku config:set DBPASSWORD=<>
```

* When deploying, it is best to set `DEBUG` is set to `False`
```
heroku config:set DEBUG=False
```

```
git status
git add 
git push origin master 
git push heroku master
```

* Set up a database
```
heroku addons:create heroku-postgresql:hobby-dev
heroku run python3.6 manage.py check
heroku run python3.6 manage.py makemigrations *myapp*
heroku run python3.6 manage.py migrate
heroku run python3.6 manage.py createsuperuser
``` 

* Open the site, hopefully it works for you
```
heroku open
```

## Contributions
Contrubutions for updating this are welcomed.





