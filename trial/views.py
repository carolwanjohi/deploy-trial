from django.shortcuts import render
from .models import Trial

# Create your views here.
def index(request):
    title = 'Deploy Trial'
    message = 'Deploying revamped'
    trials = Trial.objects.all();
    return render(request, 'index.html',{"title":title, "message":message, "trials":trials})