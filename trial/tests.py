from django.test import TestCase
from .models import Trial

# Create your tests here.

class TrialTestClass(TestCase):
    '''
    Test case for Trial class
    '''

    # Set Up method
    def setUp(self):
        '''
        Method that sets up a Trial instance before each test
        '''
        # Create a Trial instance
        self.new_trial = Trial(name='Python')

    def test_instance(self):
        '''
        Test case to check if self.new_trial in an instance of Trial class
        '''
        self.assertTrue( isinstance(self.new_trial, Trial) )